﻿Imports Newtonsoft.Json.Linq
Imports System.IO
Imports System.Net.Sockets

Public Class frmCaller

    ' Variable object dataset
    Dim ds As New ds

    ' Variable tcp sebagai client display dan printer
    Dim tcDisplay, tcPrinter As TcpClient
    Dim nsDisplay, nsPrinter As NetworkStream
    Dim bwDisplay, bwPrinter As BinaryWriter
    Dim brDisplay, brPrinter As BinaryReader

    ' Variable untuk menampung hasil split value kiriman dari server
    Dim noAntrian, idPoli, typeAntrian, idPasien As String

    Dim T_KEDATANGANTableAdapters As New dsTableAdapters.T_KEDATANGANTableAdapter

    Sub New()
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Try
            ' Baca file config
            Dim configText As String = clsConfig.Load()

            ' Tampung pada object json
            Dim jsonSettings As JObject = JObject.Parse(configText.Replace(Chr(10), Chr(13)))

            ' Simpan nilai file config ke variable property
            My.Settings.Item("NamaInstansi") = jsonSettings("Instansi")("Nama").ToString
            My.Settings.Item("AlamatInstansi") = jsonSettings("Instansi")("Alamat").ToString
            My.Settings.Item("DiplayStationIP") = jsonSettings("DiplayStation")("IP").ToString
            My.Settings.Item("DiplayStationPort") = jsonSettings("DiplayStation")("Port").ToString
            My.Settings.Item("PrinterStationIP") = jsonSettings("PrinterStation")("IP").ToString
            My.Settings.Item("PrinterStationPort") = jsonSettings("PrinterStation")("Port").ToString
            My.Settings.Item("IDPoli") = jsonSettings("IDPoli").ToString
            My.Settings.Item("ConnectionString") = String.Format("Data Source={0};Initial Catalog={1};Persist Security Info=False;User ID=sa;Password=S3cr3t", jsonSettings("DB")("Host").ToString, jsonSettings("DB")("Name").ToString)
        Catch ex As Exception
            MessageBox.Show(String.Format("Error on Sub New: {0}", ex.Message), "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End
        End Try
    End Sub

    ' Fungsi untuk merefresh urutan nomor antrian
    Private Sub RefreshQueue()
        Dim dr() As DataRow = ds.Queue.Select("", "Prioritas DESC, NoAntrian ASC")

        txtCurrentQueue.Text = ""
        txtNextQueue.Text = ""

        Dim index As Integer = 1
        For Each row In dr
            If index > 2 Then
                Exit For
            End If

            Select Case index
                Case 1
                    txtCurrentQueue.Text = row("NoAntrian")
                Case 2
                    txtNextQueue.Text = row("NoAntrian")
            End Select

            index += 1
        Next

        lblTotalAntrian.Text = ds.Queue.Rows.Count + ds.Done.Rows.Count
        lblSisaAntrian.Text = ds.Queue.Rows.Count
    End Sub
    Dim M_POLITableAdapters As New dsTableAdapters.M_POLITableAdapter
    Private Sub frmCaller_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        Try
            ' Untuk keperluan komunikasi tcp
            CheckForIllegalCrossThreadCalls = False

            Dim dr() As DataRow = M_POLITableAdapters.GetData().Select(String.Format("IDPOLI = '{0}'", My.Settings.IDPoli))
            If dr.Length > 0 Then
                T_KEDATANGANTableAdapters.Fill(ds.T_KEDATANGAN, Today, Now, My.Settings.IDPoli)
            Else
                T_KEDATANGANTableAdapters.FillEmptyIDPasien(ds.T_KEDATANGAN, Today, Now)
            End If


            ' Masukkan data ke datatable dataset
            ds.Queue.NewRow()
            For Each row In ds.T_KEDATANGAN
                ds.Queue.Rows.Add(row("NOANTRIAN"), IIf(row("TIPEANTRIAN") = "PRIORITAS", 1, 0))
            Next

            'ds.Queue.Rows.Add("C001", 0)
            'ds.Queue.Rows.Add("C002", 1)
            'ds.Queue.Rows.Add("C003", 0)
            'ds.Queue.Rows.Add("C004", 0)
            'ds.Queue.Rows.Add("C005", 0)
            'ds.Queue.Rows.Add("C006", 0)
            'ds.Queue.Rows.Add("C007", 0)

            ' Refresh data antrian
            RefreshQueue()

            ' Jalankan background worker
            'BackgroundWorker1.RunWorkerAsync()
            BackgroundWorker2.RunWorkerAsync()
        Catch ex As Exception
            MessageBox.Show(String.Format("Error on Sub New: {0}", ex.Message), "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End
        End Try
    End Sub

    Private Sub btnSelanjutnya_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSelanjutnya.Click
        Try
            ' Ketika tombol selanjutnya ditekan
            ' No antrian yang saat ini akan dihapus
            ' Dari datatable temporary
            Dim dr() As DataRow = ds.Queue.Select(String.Format("NoAntrian = '{0}'", txtCurrentQueue.Text))
            If dr.Length > 0 Then
                ' Pindah ke datatable done
                ds.Done.NewRow()
                ds.Done.Rows.Add(dr(0)("NoAntrian"))

                ' Hapus dari datatable queue
                dr(0).Delete()
                ds.Queue.AcceptChanges()

                ' Refresh data antrian
                RefreshQueue()

                ' Jika control txtCurrentQueue masih ada value
                ' Makan kirim data ke client
                If txtCurrentQueue.Text <> "" Then
                    tcDisplay = New TcpClient()
                    tcDisplay.Connect(My.Settings.DiplayStationIP, My.Settings.DiplayStationPort)
                    nsDisplay = tcDisplay.GetStream
                    bwDisplay = New BinaryWriter(nsDisplay)
                    bwDisplay.Write(String.Format("{0};{1}", txtCurrentQueue.Text, My.Settings.IDPoli))
                    nsDisplay.Close()
                    tcDisplay.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Sub btnSelanjutnya_Click!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub btnPanggil_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPanggil.Click
        Try
            If txtCurrentQueue.Text <> "" Then
                tcDisplay = New TcpClient()
                tcDisplay.Connect(My.Settings.DiplayStationIP, My.Settings.DiplayStationPort)
                nsDisplay = tcDisplay.GetStream
                bwDisplay = New BinaryWriter(nsDisplay)
                bwDisplay.Write(String.Format("{0};{1}", txtCurrentQueue.Text, My.Settings.IDPoli))
                nsDisplay.Close()
                tcDisplay.Close()
            End If

            'BackgroundWorker3.RunWorkerAsync()

            'Dim frm As New Form2(txtCurrentQueue.Text)
            'frm.Show()

            'If lblDisplayConnectionStatus.Text = "Connected" Then
            '    ' Jika control txtCurrentQueue masih ada value
            '    ' Makan kirim data ke client
            '    If txtCurrentQueue.Text <> "" Then
            '        bwDisplay.Write(String.Format("{0};{1}", txtCurrentQueue.Text, My.Settings.IDPoli))
            '    End If
            'Else
            '    MessageBox.Show(String.Format("Tidak terkoneksi dengan Display Station. Panggilan tidak dapat dilakukan."), "Peringatan!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            'End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Sub btnPanggil_Click!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    ' Event ini digunakan untuk memastikan
    ' Koneksi dengan server Display selalu terjaga
    Private Sub BackgroundWorker1_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
RestartConnection:

        ' Restart service tcp client
        ' Agar terkonek kembali dengan server Display
        tcDisplay = New TcpClient()
        Do Until tcDisplay.Connected
            Try
                tcDisplay.Connect(My.Settings.DiplayStationIP, My.Settings.DiplayStationPort)
                nsDisplay = tcDisplay.GetStream
                bwDisplay = New BinaryWriter(nsDisplay)
                brDisplay = New BinaryReader(nsDisplay)
            Catch ex As Exception
                Console.WriteLine(ex.Message)
            End Try
        Loop
        Console.WriteLine("Connected")

        ' Setelah konek dengan server Display
        ' Terus membaca sampai koneksi terputus dengan server
        Do Until Not tcDisplay.Connected
            Try
                lblDisplayConnectionStatus.Text = "Connected"
                Console.WriteLine(brDisplay.ReadString)
            Catch ex As Exception
                If ex.Message.Contains("Unable to read beyond the end of the stream.") Then
                    nsDisplay.Close()
                    tcDisplay.Close()
                Else
                    Console.WriteLine(ex.Message)
                End If
            End Try
        Loop
        lblDisplayConnectionStatus.Text = "Disconnect"
        Console.WriteLine("Disconnected")

        ' Restart service tcp client
        GoTo RestartConnection
    End Sub

    ' Event ini digunakan untuk memastikan
    ' Koneksi dengan server Printer selalu terjaga
    Private Sub BackgroundWorker2_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker2.DoWork
RestartConnection:

        ' Restart service tcp client
        ' Agar terkonek kembali dengan server Printer
        tcPrinter = New TcpClient()
        Do Until tcPrinter.Connected
            Try
                tcPrinter.Connect(My.Settings.PrinterStationIP, My.Settings.PrinterStationPort)
                nsPrinter = tcPrinter.GetStream
                bwPrinter = New BinaryWriter(nsPrinter)
                brPrinter = New BinaryReader(nsPrinter)
            Catch ex As Exception
                Console.WriteLine(ex.Message)
            End Try
        Loop
        Console.WriteLine("Connected")
        Dim value As String

        ' Setelah konek dengan server Printer
        ' Terus membaca sampai koneksi terputus dengan server
        Do Until Not tcPrinter.Connected
            Try
                ' Update koneksi status ke printer station
                lblPrinterConnectionStatus.Text = "Connected"

                ' Tangkap value dari printer station
                value = brPrinter.ReadString

                ' Split value menjadi array
                Dim str() As String = value.Split(";")

                ' Tulis di console
                Console.WriteLine(value)

                Try
                    ' Set variable value
                    noAntrian = str(0)
                    idPoli = str(1)
                    typeAntrian = IIf(str(2) = "PRIORITAS", 1, 0)
                    idPasien = str(3)

                    ' Cek apakah polinya sama
                    ' Jika sama maka dimasukkan / diupdate
                    ' Ke list antrian sesuai poli masing-masing
                    Dim dr As DataRow() = ds.M_POLI.Select(String.Format("IDPOLI = '{0}'", My.Settings.IDPoli))
                    If dr.Length = 0 And idPasien = "-" Then

                        ' Jika ini aplikasi caller
                        ' Dan idPasien bernilai "-"
                        ' Maka tambahkan ke list antrian
                        ds.Queue.NewRow()
                        ds.Queue.Rows.Add(noAntrian, typeAntrian)

                        RefreshQueue()
                    ElseIf idPoli = My.Settings.IDPoli And idPasien <> "-" Then

                        ' Cari apakah NoAntrian sudah ada di list
                        dr = ds.Queue.Select(String.Format("NoAntrian = '{0}'", noAntrian))

                        If dr.Length > 0 Then
                            ' Jika sudah ada maka update prioritas
                            dr(0)("Prioritas") = typeAntrian
                            ds.Queue.AcceptChanges()
                        Else
                            ' Jika tidak ada maka tambahkan ke list antrian
                            ds.Queue.NewRow()
                            ds.Queue.Rows.Add(noAntrian, typeAntrian)
                        End If

                        RefreshQueue()
                    End If
                Catch ex As Exception
                    Console.WriteLine(ex.Message)
                End Try
            Catch ex As Exception
                If ex.Message.Contains("Unable to read beyond the end of the stream.") Then
                    nsPrinter.Close()
                    tcPrinter.Close()
                Else
                    Console.WriteLine(ex.Message)
                End If
            End Try
        Loop
        lblPrinterConnectionStatus.Text = "Disconnect"
        Console.WriteLine("Disconnected")

        ' Restart service tcp client
        GoTo RestartConnection
    End Sub
End Class