﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmCaller
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btnPanggil = New System.Windows.Forms.Button()
        Me.txtCurrentQueue = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnSelanjutnya = New System.Windows.Forms.Button()
        Me.txtNextQueue = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblTotalAntrian = New System.Windows.Forms.Label()
        Me.lblSisaAntrian = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.lblDisplayConnectionStatus = New System.Windows.Forms.Label()
        Me.BackgroundWorker2 = New System.ComponentModel.BackgroundWorker()
        Me.lblPrinterConnectionStatus = New System.Windows.Forms.Label()
        Me.BackgroundWorker3 = New System.ComponentModel.BackgroundWorker()
        Me.SuspendLayout()
        '
        'btnPanggil
        '
        Me.btnPanggil.Location = New System.Drawing.Point(12, 121)
        Me.btnPanggil.Name = "btnPanggil"
        Me.btnPanggil.Size = New System.Drawing.Size(130, 23)
        Me.btnPanggil.TabIndex = 0
        Me.btnPanggil.Text = "Ulangi Panggilan"
        Me.btnPanggil.UseVisualStyleBackColor = True
        '
        'txtCurrentQueue
        '
        Me.txtCurrentQueue.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCurrentQueue.Location = New System.Drawing.Point(12, 24)
        Me.txtCurrentQueue.Name = "txtCurrentQueue"
        Me.txtCurrentQueue.Size = New System.Drawing.Size(130, 62)
        Me.txtCurrentQueue.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(82, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Antrian Saat Ini:"
        '
        'btnSelanjutnya
        '
        Me.btnSelanjutnya.Location = New System.Drawing.Point(12, 92)
        Me.btnSelanjutnya.Name = "btnSelanjutnya"
        Me.btnSelanjutnya.Size = New System.Drawing.Size(130, 23)
        Me.btnSelanjutnya.TabIndex = 3
        Me.btnSelanjutnya.Text = "Selanjutnya"
        Me.btnSelanjutnya.UseVisualStyleBackColor = True
        '
        'txtNextQueue
        '
        Me.txtNextQueue.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNextQueue.Location = New System.Drawing.Point(148, 51)
        Me.txtNextQueue.Name = "txtNextQueue"
        Me.txtNextQueue.Size = New System.Drawing.Size(69, 35)
        Me.txtNextQueue.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(148, 35)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(101, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Antrian Selanjutnya:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(41, 179)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Total Antrian:"
        '
        'lblTotalAntrian
        '
        Me.lblTotalAntrian.AutoSize = True
        Me.lblTotalAntrian.Location = New System.Drawing.Point(117, 179)
        Me.lblTotalAntrian.Name = "lblTotalAntrian"
        Me.lblTotalAntrian.Size = New System.Drawing.Size(13, 13)
        Me.lblTotalAntrian.TabIndex = 7
        Me.lblTotalAntrian.Text = "0"
        '
        'lblSisaAntrian
        '
        Me.lblSisaAntrian.AutoSize = True
        Me.lblSisaAntrian.Location = New System.Drawing.Point(117, 202)
        Me.lblSisaAntrian.Name = "lblSisaAntrian"
        Me.lblSisaAntrian.Size = New System.Drawing.Size(13, 13)
        Me.lblSisaAntrian.TabIndex = 9
        Me.lblSisaAntrian.Text = "0"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(41, 202)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(66, 13)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Sisa Antrian:"
        '
        'BackgroundWorker1
        '
        '
        'lblDisplayConnectionStatus
        '
        Me.lblDisplayConnectionStatus.AutoSize = True
        Me.lblDisplayConnectionStatus.Location = New System.Drawing.Point(145, 102)
        Me.lblDisplayConnectionStatus.Name = "lblDisplayConnectionStatus"
        Me.lblDisplayConnectionStatus.Size = New System.Drawing.Size(0, 13)
        Me.lblDisplayConnectionStatus.TabIndex = 10
        '
        'BackgroundWorker2
        '
        '
        'lblPrinterConnectionStatus
        '
        Me.lblPrinterConnectionStatus.AutoSize = True
        Me.lblPrinterConnectionStatus.Location = New System.Drawing.Point(145, 121)
        Me.lblPrinterConnectionStatus.Name = "lblPrinterConnectionStatus"
        Me.lblPrinterConnectionStatus.Size = New System.Drawing.Size(0, 13)
        Me.lblPrinterConnectionStatus.TabIndex = 11
        '
        'BackgroundWorker3
        '
        '
        'frmCaller
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(259, 244)
        Me.Controls.Add(Me.lblPrinterConnectionStatus)
        Me.Controls.Add(Me.lblDisplayConnectionStatus)
        Me.Controls.Add(Me.lblSisaAntrian)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.lblTotalAntrian)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtNextQueue)
        Me.Controls.Add(Me.btnSelanjutnya)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtCurrentQueue)
        Me.Controls.Add(Me.btnPanggil)
        Me.Name = "frmCaller"
        Me.Text = "frmCaller"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnPanggil As Button
    Friend WithEvents txtCurrentQueue As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents btnSelanjutnya As Button
    Friend WithEvents txtNextQueue As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents lblTotalAntrian As Label
    Friend WithEvents lblSisaAntrian As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents lblDisplayConnectionStatus As Label
    Friend WithEvents BackgroundWorker2 As System.ComponentModel.BackgroundWorker
    Friend WithEvents lblPrinterConnectionStatus As System.Windows.Forms.Label
    Friend WithEvents BackgroundWorker3 As System.ComponentModel.BackgroundWorker
End Class
