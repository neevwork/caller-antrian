﻿Imports System.Net.Sockets
Imports System.IO

Public Class Form2
    Dim tcDisplay, tcPrinter As TcpClient
    Dim nsDisplay, nsPrinter As NetworkStream
    Dim bwDisplay, bwPrinter As BinaryWriter
    Dim brDisplay, brPrinter As BinaryReader
    Dim _noAntrian As String
    Sub New(ByVal noantrian As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _noAntrian = noantrian
    End Sub
    Private Sub Form2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        tcDisplay = New TcpClient()
        nsDisplay = tcDisplay.GetStream
        bwDisplay = New BinaryWriter(nsDisplay)
        bwDisplay.Write(String.Format("{0};{1}", _noAntrian, My.Settings.IDPoli))
        Me.Close()
    End Sub
End Class